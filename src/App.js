import React, { useState } from 'react';

import Table from './Components/Table';
import Input from './Components/Input';
import Header from './Components/Header';

const App = () => {
  const [nameList, setNameList] = useState([]);

  const nameRef = React.createRef();
  
  const handleSubmitClick = () => {
    // fill in with your logic to handle addition of names
    let value = nameRef.current.value;
    let items = [...nameList];
    if(!validate(value)) {
      return;
    }
    items.push(value);
    //sorts items to alphabetical order
    items.sort((a, b) => 
      String(a).localeCompare(String(b))
    ).map((item, i) => <tr><td key={i}>{item}</td></tr>);
    setNameList(items);
  };

  function validate(input) {
    if (input === "") {
        alert("Enter a name");
        return false;
    }
    if (!/^[a-zA-Z]*$/g.test(input)) {
        alert("Invalid characters");
        return false;
    }
    else {
      return true;
    }
}

  return (
    <div className="container">
      <Header heading="Employees" />
      <Table names={nameList} />
      <div className="input">
        <Input ref={nameRef}/>
        <input type="button" value='Add' onClick={handleSubmitClick} />
      </div>
    </div>
  );
}

export default App;
