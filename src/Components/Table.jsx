import React from 'react';



const Table = ({ names }) => {
  console.log('names:', names);
  return (
    <>
    <div>
      <table>
        <thead>
          <tr>
            <th>Names</th>
          </tr>
        </thead>
        <tbody>
          {
            names.map((item, i) => <tr><td key={i}>{item}</td></tr>) //Maps items in names to rows in table
          }
        </tbody>
      </table>
    </div>
    </>
  )
};

export default Table;
