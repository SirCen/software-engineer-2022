import React from 'react';

const Header = ({ heading }) => {
    return (
      <>
      <div>
          <h2>{heading}</h2>
      </div>
      </>
    );
}

export default Header;