import React from 'react';


//Using react.forwardRef to allow the ref to be accessed from App.js
const Input = React.forwardRef((_, ref) => {
  return (
    <input type="text" placeholder="Enter Employee Name" ref={ref} />
  )
});

export default Input;
